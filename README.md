# Paint

The infamous CS-250 Pain(t) project.

## Purpose

A class exercise in implementing a (loose) clone of Microsoft Paint using JavaFX.

## Pictures

Update Check

![Update Check](photos/updates.png)

Release Notes and Polygon Prompt

![Release Notes and Polygon Prompt](photos/release_notes.png)

Tabbing and Image Modification

![Tabbing and Image Modification](photos/tabbing.png)

## Versioning

Paint follows Semantic Versioning according to: https://semver.org/.

You can view the current version and release notes in the "src/main/resources/releases" folder.

## Copyright and Crediting Sources

### Initial Canvas Image

[Image](https://flickr.com/photos/glaciernps/51510834042/) taken by Glacier National Park. Image is in the public domain.

### Paint 

Copyright (c) 2021 Nathan Harmon
