/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.drawing;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import net.nathanharmon.paint.canvas.CopyAndMove;
import net.nathanharmon.paint.canvas.CutAndMove;
import net.nathanharmon.paint.canvas.Draw;

import java.util.Stack;

/**
 * Tab with proper Canvas under it.
 */
public class CanvasTab extends Tab {

    private final Canvas canvas;
    private final CanvasActions canvasActions;

    // X and Y of when a mouse is pressed, stored until mouse is released.
    private double x, y;

    private final Stack<Image> previousActions;
    private final Stack<Image> forwardActions;

    private Image original;

    protected CanvasTab(TabManager tabs, String title) {
        super(title);

        canvas = new Canvas();
        canvasActions = new CanvasActions(canvas);
        ScrollPane scrollPane = new ScrollPane();

        previousActions = new Stack<>();
        forwardActions = new Stack<>();

        scrollPane.setContent(canvas);
        setContent(scrollPane);

        /*
         * Handle Mouse Pressing for Tools
         */
        getCanvas().setOnMousePressed(event -> {
            x = event.getX();
            y = event.getY();

            // save original image to go back to during the live draw
            original = getActions().getImage();
        });

        getCanvas().setOnMouseDragged(event -> {
            if(!(tabs.getSelectedTool() instanceof CutAndMove) && // if the current tool is either CutAndMove or Draw, then skip. This is a hardcoded list of tools where it doesn't make sense to have a live preview.
                    !(tabs.getSelectedTool() instanceof Draw) &&
                    !(tabs.getSelectedTool() instanceof CopyAndMove)) {
                // restore original image to canvas
                getActions().drawImage(original, 0, 0);

                // draw shape for live draw
                tabs.getSelectedTool().draw(x, y, event.getX(), event.getY());
            } else {
                tabs.getSelectedTool().drag(event.getX(), event.getY());

                // save changes applied during the drag function
                original = getActions().getImage();
            }
        });

        getCanvas().setOnMouseReleased(event -> {
            // restore old image before sending action to tool code
            getActions().drawImage(original, 0, 0);

            // handle image saving for use in undo and redo
            addAction();

            // send action to tool
            tabs.getSelectedTool().draw(x, y, event.getX(), event.getY());
        });
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public CanvasActions getActions() {
        return canvasActions;
    }

    /*
    Undo/Redo
    */

    /**
     * Perform an undo action on the canvas.
     */
    public void undo() {
        if(!previousActions.empty()) {
            Image current = getActions().getImage();
            Image image = previousActions.pop();

            // add image to redo stack
            forwardActions.push(current);

            // set canvas to what the previous picture was
            getActions().drawImage(image, 0, 0);
        }
    }

    /**
     * Perform a redo action on the canvas.
     */
    public void redo() {
        if(!forwardActions.empty()) {
            Image current = getActions().getImage();
            Image image = forwardActions.pop();

            // add image to undo stack
            previousActions.push(current);

            // set canvas to what the next picture was
            getActions().drawImage(image, 0, 0);
        }
    }

    /**
     * Add image to undo list.
     */
    public void addAction() {
        Image image = canvasActions.getImage();
        previousActions.push(image);
        forwardActions.clear();
    }
}
