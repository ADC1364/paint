/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import javafx.scene.Cursor;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import javafx.scene.robot.Robot;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

import java.util.logging.Level;

/**
 * Select color from the screen and import it into the color options.
 */
public class DropperTool extends CanvasTool {

    private final ColorPicker picker;

    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public DropperTool(ColorPicker picker, TabManager tabs) {
        super(tabs, Language.getText("dropper"), "dropper");
        this.picker = picker;

        // override action event from CanvasTool
        setOnAction(event -> {
            tabs.setSelectedTool(this);

            // on press, change cursor to crosshair
            tabs.setCursor(Cursor.CROSSHAIR);

            Main.log(Level.INFO, "Switched tool to %s.", getName());
        });
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        Robot robot = new Robot();

        Color c = robot.getPixelColor(robot.getMouseX(), robot.getMouseY());

        getTabs().setPaint(c);
        picker.setValue(c);
    }

    @Override
    public void drag(double x, double y) {}
}
