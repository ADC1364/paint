/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Draw a Scalene Triangle with filling.
 */
public class FilledScaleneTriangle extends CanvasTool {

    /**
     * Create new Canvas Tool
     *
     * @param tabs Tab manager object (to access the tool selection).
     */
    public FilledScaleneTriangle(TabManager tabs) {
        super(tabs, Language.getText("filled") + Language.getText("scalenetriangle"), "scalenetriangle-filled");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setFill(getTabs().getPaint());

        getGraphics().fillPolygon(new double[] {sx, sx+(ex-sx)/2, ex}, new double[] {sy, sy, ey}, 3);
    }

    @Override
    public void drag(double x, double y) {}
}
