/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Draw a rounded square without filling.
 */
public class RoundSquare extends CanvasTool {


    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public RoundSquare(TabManager tabs) {
        super(tabs, Language.getText("round") + Language.getText("square"), "round-square");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setStroke(getTabs().getPaint());

        double[] rect = calculateRect(sx, sy, ex, ey);

        getGraphics().strokeRoundRect(rect[0], rect[1], rect[2], rect[2], 10, 10);
    }

    @Override
    public void drag(double x, double y) {}
}
