/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import javafx.scene.Cursor;
import javafx.scene.control.TextInputDialog;
import javafx.util.Pair;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

import java.util.Optional;
import java.util.logging.Level;

/**
 * Draw a polygon with filling.
 */
public class FilledPolygon extends CanvasTool {

    private int sides = 5; // arbitrarily set number of sides as a default value

    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public FilledPolygon(TabManager tabs) {
        super(tabs, Language.getText("filled") + Language.getText("polygon"), "polygon-filled");

        // override action event from CanvasTool
        setOnAction(event -> {
            tabs.setSelectedTool(this);

            // reset cursor to standard cursor
            tabs.setCursor(Cursor.DEFAULT);

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle(Language.getText("polytitle"));
            dialog.setHeaderText(Language.getText("polyquestion"));

            Optional<String> result = dialog.showAndWait();

            if(result.isPresent())
                try {
                    sides = Integer.parseInt(result.get());
                } catch(NumberFormatException e) {
                    Main.log(Level.WARNING, "Failed to parse integer from user prompt.");
                }

            Main.log(Level.INFO, "Switched tool to %s.", getName());
        });
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setFill(getTabs().getPaint());

        // a polygon cannot have less than 3 sides
        if(sides > 2) {

            // Calculate the points of the polygon.
            Pair<double[], double[]> points = getPolygonPoints(sx, sy, ex, ey, sides);

            double[] xPoints = points.getKey();
            double[] yPoints = points.getValue();

            getGraphics().fillPolygon(xPoints, yPoints, sides);

        } else {
            Main.log(Level.WARNING, "Number entered is not valid. Must be greater than 2.");
        }
    }

    @Override
    public void drag(double x, double y) {}
}
