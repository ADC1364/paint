/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Erase a section of the canvas.
 */
public class Eraser extends CanvasTool {

    /**
     * Create new Canvas Tool
     *
     * @param tabs Tab manager object (to access the tool selection).
     */
    public Eraser(TabManager tabs) {
        super(tabs, Language.getText("eraser"), "eraser");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        double[] rect = calculateRect(sx, sy, ex, ey);

        getGraphics().clearRect(rect[0], rect[1], rect[2], rect[3]);
    }

    @Override
    public void drag(double x, double y) {}
}
