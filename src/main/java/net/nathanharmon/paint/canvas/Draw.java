/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Freehand draw on the canvas.
 */
public class Draw extends CanvasTool {

    /**
     * Create new Canvas Tool
     *
     * @param tabs Tab manager object (to access the tool selection).
     */
    public Draw(TabManager tabs) {
        super(tabs, Language.getText("draw"), "draw");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {}

    @Override
    public void drag(double x, double y) {
        getGraphics().setFill(getTabs().getPaint());

        double w = getTabs().getLineWidth();
        getGraphics().fillOval(x-w, y-w, w, w);
    }
}
