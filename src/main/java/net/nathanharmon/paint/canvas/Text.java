/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import javafx.scene.control.TextInputDialog;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

import java.util.Optional;

/**
 * Create text on the canvas.
 */
public class Text extends CanvasTool {

    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public Text(TabManager tabs) {
        super(tabs, Language.getText("text"), "text");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(Language.getText("texttitle"));
        dialog.setHeaderText(Language.getText("textquestion"));

        Optional<String> result = dialog.showAndWait();

        getGraphics().setFill(getTabs().getPaint());

        result.ifPresent(s -> getGraphics().fillText(s, sx, sy));
    }

    @Override
    public void drag(double x, double y) {}
}
