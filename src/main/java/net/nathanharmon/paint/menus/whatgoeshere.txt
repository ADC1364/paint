---
Project: Paint
Author: Nathan Harmon
License: Copyright (c) 2021 Nathan Harmon
Location: /src/main/java/net.nathanharmon.paint/menus/
---

Menu items which are sorted into folders according to their menu category (File, Edit, Help...).

Each folder represents a different menu category.
