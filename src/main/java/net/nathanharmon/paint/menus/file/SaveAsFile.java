/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.file;

import javafx.event.EventType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.FileUtilities;

import java.io.File;

/**
 * File -> Save As | Save while always prompting for a new file name.
 */
public class SaveAsFile extends MenuItem {

    public SaveAsFile(TabManager tabs) {
        super("Save As");
        setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        addEventHandler(EventType.ROOT, event -> {
            if(tabs.getCurrentlySelectedTab() != null) {
                // prompt user for saving location
                File file = FileUtilities.saveImage("Save as", tabs.getCurrentlySelectedTab().getText());

                // store file object
                tabs.getCurrentlySelectedTab().getActions().setSaveFile(file);

                // mark this tab as saved
                tabs.getCurrentlySelectedTab().getActions().setSaved(true);

                FileUtilities.writeImageToFile(tabs.getCurrentlySelectedTab().getActions().getImage(), file);

                tabs.getCurrentlySelectedTab().setText(file.getName());
            }
        });
    }
}
