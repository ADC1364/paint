/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;
import net.nathanharmon.paint.menus.edit.Redo;
import net.nathanharmon.paint.menus.edit.Undo;
import net.nathanharmon.paint.menus.file.*;
import net.nathanharmon.paint.menus.help.About;
import net.nathanharmon.paint.menus.help.Autosave;
import net.nathanharmon.paint.menus.help.CheckForUpdates;
import net.nathanharmon.paint.menus.help.ReleaseNotes;

/**
 * Register the menus in the header bar.
 */
public class MenuManager {
    
    private final MenuBar headerMenu;
    private final TabManager tabs;

    public MenuManager(MenuBar headerMenu, TabManager tabs) {
       this.headerMenu = headerMenu;
       this.tabs = tabs;
    }
    
    public void register() {
        // create menu categories
        Menu file = new Menu(Language.getText("file"));
        Menu edit = new Menu(Language.getText("edit"));
        Menu insert = new Menu(Language.getText("insert"));
        Menu view = new Menu(Language.getText("view"));
        Menu help = new Menu(Language.getText("help"));

        // add items to file menu
        file.getItems().addAll(
                new New(tabs),
                new OpenFile(tabs),
                new SaveFile(tabs),
                new SaveAsFile(tabs),
                new Exit(tabs));

        // add items to edit menu
        edit.getItems().addAll(
                new Undo(tabs),
                new Redo(tabs));

        // add items to help menu
        help.getItems().addAll(new ReleaseNotes(), new CheckForUpdates(), new Autosave(tabs), new About());

        // add menu categories to the menu bar
        headerMenu.getMenus().addAll(file, edit, insert, view, help);
    }
}
