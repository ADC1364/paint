/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.help;

import javafx.event.EventType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Config;
import net.nathanharmon.paint.helper.file.FileUtilities;
import net.nathanharmon.paint.helper.file.Language;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Help -> Autosave | Set the time delay of autosave.
 */
public class Autosave extends MenuItem {

    public Autosave(TabManager tabs) {
        super("Modify Autosave Interval");

        addEventHandler(EventType.ROOT, event -> {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle(Language.getText("autosavetitle"));
            dialog.setHeaderText(Language.getText("autosavequestion"));

            Optional<String> result = dialog.showAndWait();

            AtomicLong num = new AtomicLong();
            result.ifPresent(s -> num.set(Long.parseLong(s) * 60000));

            tabs.setAutosaveInterval(num.get());

            Config config = new Config(new File(FileUtilities.getCacheFolder() + "data.yaml"));

            config.writeData("autosaveInterval", String.valueOf(num.get()));
        });
    }
}
