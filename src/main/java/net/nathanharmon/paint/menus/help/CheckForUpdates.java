/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.help;

import io.herrera.kevin.semver.InvalidVersionException;
import io.herrera.kevin.semver.Version;
import javafx.event.EventType;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import net.nathanharmon.paint.Main;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;

/**
 * Help -> Check For Updates | Check if the current version is less than the most recent version on Git.
 */
public class CheckForUpdates extends MenuItem {

    public CheckForUpdates() {
        super("Check For Updates");

        addEventHandler(EventType.ROOT, event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

            // add menu text
            alert.setHeaderText("Update Check");
            alert.setTitle("Check for Updates");

            try {
                URL url = new URL("https://gitlab.com/nathanharmon/paint/-/raw/master/src/main/resources/version.txt");
                Scanner in = new Scanner(url.openStream());

                Version latest = new Version(in.next());
                Version current = new Version(Main.VERSION);

                if(latest.isGreaterThan(current))
                    alert.setContentText("You have a new update available: " + current + " -> " + latest);
                else
                    alert.setContentText("You are on the latest version: " + current);

            } catch(IOException e) {
                Main.log(Level.WARNING, "Failed to download latest version from website.");
            } catch (InvalidVersionException e) {
                Main.log(Level.SEVERE, "There was an error parsing the version.");
            }

            alert.showAndWait();
        });
    }
}
