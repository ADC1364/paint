/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.help;

import net.nathanharmon.paint.menus.MenuItemToWindow;

/**
 * Help -> About | Show information about the current version of Pain(t).
 */
public class About extends MenuItemToWindow {

    public About() {
        super("About", "About Pain(t)", "about-window.fxml");
    }
}
