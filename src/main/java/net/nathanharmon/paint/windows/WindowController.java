/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.windows;

public abstract class WindowController {

   public abstract void onOpen();
}
