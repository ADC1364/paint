/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.windows;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.helper.file.FileUtilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

/**
 * The controller class for the release notes window.
 */
public class ReleaseNotesWindow extends WindowController {

    @FXML
    private TextArea notes;

    @Override
    public void onOpen() {
       /*
        * Display the current release notes in text area.
        */
        InputStream latest = FileUtilities.getResourcesStream("latest.txt");
        String text = "";

        try {
            text = turnStreamToString(latest);
        } catch (IOException e) {
            Main.log(Level.SEVERE, "Error converting a stream to a string for the release notes.");
        }

        notes.setText(text);
    }

    /**
     * Convert an input stream to a string.
     * Adapted from: https://stackoverflow.com/a/1891688
     * @param stream InputStream in UTF-8 encoding.
     * @return A string created out of the input stream.
     */
    private String turnStreamToString(InputStream stream) throws IOException {
        Reader r;
        char[] buf = new char[2048];

        r = new InputStreamReader(stream, StandardCharsets.UTF_8);

        StringBuilder builder = new StringBuilder();

        int n;
        do {
            n = r.read(buf);

            if(n >= 0)
                builder.append(buf, 0, n);
        } while(n >= 0);

        return builder.toString();
    }
}
