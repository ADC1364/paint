/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.windows;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.helper.file.Language;

/**
 * The  controller class for the "about window", the window that has information about Pain(t).
 */
public class AboutWindow extends WindowController {

    @FXML
    private Label version;

    @Override
    public void onOpen() {
        version.setText(Language.getText("version") + Main.VERSION);
    }
}
