/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.windows;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.canvas.CanvasToolManager;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.Autosave;
import net.nathanharmon.paint.helper.file.FileUtilities;
import net.nathanharmon.paint.helper.file.Language;
import net.nathanharmon.paint.menus.MenuManager;

import java.io.InputStream;
import java.util.Timer;
import java.util.logging.Level;

/**
 * The controller class for the main window, the window that has the canvas on it.
 */
public class MainWindow {

    @FXML
    private MenuBar headerMenu;

    @FXML
    private SplitPane splitPane;

    @FXML
    private TabPane tabPane;

    @FXML
    private GridPane toolPane;

    @FXML
    private Slider thicknessSlider;

    @FXML
    private ColorPicker colorChooser;

    @FXML
    private Slider zoomSlider;

    @FXML
    private ProgressBar saveProgress;

    @FXML
    private Label selectedTool;
    
    private TabManager tabManager;

    /**
     * When window is first opened, run.
     * @param stage The JavaFX stage for the main window.
     */
    public void onOpen(Stage stage) {
        tabManager = new TabManager(tabPane, stage, selectedTool);
        tabManager.setCurrentlySelectedTab(tabManager.createTab(Language.getText("untitled"), true));

        // autosave interval
        long interval = tabManager.getAutosaveInterval();

        // Start autosave task
        String path = FileUtilities.getCacheFolder();
        Main.log(Level.INFO, "Autosave is set to save in the %s folder. Saving every %d milliseconds.", path, interval);
        if(path != null) {
            Timer timer = new Timer();
            Autosave autosave = new Autosave(tabManager, path, this);
            timer.scheduleAtFixedRate(autosave, interval, interval); // by default: wait 2 minutes to save, then wait 2 minutes between save checks
        }

        /*
        Load initial tab with an intro picture: intro.png.

        Fail behavior: The tab is still created, but the image is not loaded into it.
         */
        InputStream initialImage = FileUtilities.getResourcesStream("images/intro.png");
        if (initialImage != null)
            tabManager.getCurrentlySelectedTab().getActions().drawInitialImage(initialImage);
        else
            Main.log(Level.WARNING, "Failed to load initial image for tab.");

        // setup tool selected section
        selectedTool.setText(getTabManager().getSelectedTool().getName());

        // register tools in grid
        CanvasToolManager canvasToolManager = new CanvasToolManager(toolPane, getTabManager(), colorChooser);
        canvasToolManager.register();

        // add header menu items
        MenuManager menuManager = new MenuManager(headerMenu, getTabManager());
        menuManager.register();

        // set window icon
        FileUtilities.setIcon(stage);

        // Set split pane divisions
        stage.widthProperty().addListener((observableValue, o, t1) -> splitPane.setDividerPositions(0.15F, 0.85F));

        // Make split pane take up the entire window's height
        stage.heightProperty().addListener((observableValue, o, t1) -> splitPane.setMinHeight(stage.getHeight()-headerMenu.getHeight()-45));

        // prompt user to save each tab before exiting program
        stage.setOnCloseRequest(event -> tabManager.safeExit());

        // Handle thickness changes
        thicknessSlider.setValue(getTabManager().getLineWidth());
        thicknessSlider.valueProperty().addListener((observable, oldValue, newValue) -> getTabManager().setLineWidth(newValue.doubleValue()));

        // Handle Color Changes
        colorChooser.setValue(Color.BLACK); // initial value set in Canvas actions
        colorChooser.valueProperty().addListener((observable, oldValue, newValue) -> getTabManager().setPaint(newValue));

        // Handle Zoom Changes
        zoomSlider.valueProperty().addListener(((observable, oldValue, newValue) -> tabManager.setZoom(newValue.doubleValue())));
    }

    public TabManager getTabManager() {
        return tabManager;
    }

    /**
     * Display the save animation for the current tab.
     */
    public void animateSaveTab() throws InterruptedException {
        Timeline task = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(saveProgress.progressProperty(), 0)),
                new KeyFrame(Duration.seconds(2), new KeyValue(saveProgress.progressProperty(), 1))
        );

        task.playFromStart();
        saveProgress.setProgress(0);
    }
}