/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.helper.file;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.nathanharmon.paint.IdeMain;
import net.nathanharmon.paint.Main;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.jar.JarFile;
import java.util.logging.Level;

/**
 * Wrapper class for all file pickers.
 */
public class FileUtilities {

    /**
     * Get a user selected image file.
     * @param title Title of the chooser window.
     * @return File object of the selected image.
     */
   public static File getImageFile(String title) {
       // create file chooser dialog
       FileChooser fileChooser = new FileChooser();
       fileChooser.setTitle(title);
       fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));

       // add file extension filters
       fileChooser.getExtensionFilters().addAll(
               new FileChooser.ExtensionFilter("All Images", "*.*"),
               new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg"),
               new FileChooser.ExtensionFilter("PNG", "*.png"),
               new FileChooser.ExtensionFilter("HEIC", "*.heif", "*.heifs", "*.heic", "*.heics"),
               new FileChooser.ExtensionFilter("WebP", "*.webp"),
               new FileChooser.ExtensionFilter("SVG", "*.svg"),
               new FileChooser.ExtensionFilter("GIF", "*.gif"),
               new FileChooser.ExtensionFilter("Bitmap", "*.bmp")
                    /* TODO: SUPPORT FARBFELD
                     new FileChooser.ExtensionFilter("Farbfeld", "*.ff") */
       );

       return fileChooser.showOpenDialog(new Stage());
   }

    /**
     * Get the path of a to-be-created image file.
     * @param title Title of the chooser window.
     * @param fileName Default file name in the window.
     * @return A file with extension object of where to save the image to and its extension.
     */
   public static File saveImage(String title, String fileName) {
       // create file chooser dialog
       FileChooser fileChooser = new FileChooser();
       fileChooser.setTitle(title);
       fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
       fileChooser.setInitialFileName(fileName);

       return fileChooser.showSaveDialog(new Stage());
   }

    /**
     * Send the user a dialog asking if they want to save before exiting the file.
     * @param title Filename that is about to be closed.
     * @return ClosingPromptResponse detailing the user's response to the dialog.
     * @see ClosingPromptResponse
     */
   public static ClosingPromptResponse promptToSaveOnClose(@Nullable String title) {
       Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

       // add menu text
       alert.setHeaderText(Language.getText("unsavedheader"));
       alert.setContentText(String.format(Language.getText("unsavedquestion"), title == null ? Language.getText("untitled") : title));
       alert.setTitle(Language.getText("unsavedtitle"));

       // add 3 button options: save, exit, and cancel
       ButtonType save = new ButtonType(Language.getText("savebutton"));
       ButtonType exit = new ButtonType(Language.getText("exitbutton"));
       ButtonType cancel = new ButtonType(Language.getText("cancelbutton"), ButtonBar.ButtonData.CANCEL_CLOSE);

       // add buttons to the menu
       alert.getButtonTypes().setAll(save, exit, cancel);

       Optional<ButtonType> response = alert.showAndWait();
       if(response.isPresent())
           if (response.get() == save)
               return ClosingPromptResponse.SAVE;
           else if(response.get() == exit)
               return ClosingPromptResponse.EXIT;
           else // button is cancel
               return ClosingPromptResponse.CANCEL;
       else
           return ClosingPromptResponse.CANCEL; // default option (if the window is closed without an option selected)
   }

    /**
     * A user's response to the prompt asking a user if they would like to save changes before closing.
     * SAVE: Yes, save the file and then exit.
     * EXIT: Exit, and do not save changes.
     * CANCEL: Go back, neither save nor close. Also occurs when the user simply closes the prompt window.
     * @see #promptToSaveOnClose(String)
     */
   public enum ClosingPromptResponse {
       SAVE, EXIT, CANCEL
   }

    /**
     * Write an image to a file.
     * @param image Image to be written.
     * @param file File (with path) to be written to. It can already exist, but will be overwritten.
     */
    public static void writeImageToFile(Image image, File file) {
        // create parent directories if needed
        writeParentFolders(file);

        // process images to save to file
        String extension = FilenameUtils.getExtension(file.toString());

        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);

        BufferedImage saveImage = new BufferedImage((int) image.getWidth(), (int) image.getHeight(), BufferedImage.OPAQUE);
        saveImage.createGraphics().drawImage(bufferedImage, 0, 0, null);

        try {
            ImageIO.write(saveImage, extension, file);

            Main.log(Level.INFO, "Saving file %s.", file.getAbsolutePath());
        } catch (IOException e) {
            Main.log(Level.WARNING, "Error saving image to file.");
        }
    }

    /**
     * Create the parent folders of the file if they need to be created.
     * @param file This file's parent directories will be created.
     */
    public static void writeParentFolders(@NotNull File file) {
        //noinspection ResultOfMethodCallIgnored
        file.getParentFile().mkdirs();
    }

    /**
     * Get a file from the maven "resources folder" including subfolders, but not sequential subfolders.
     * @param name File name to search for.
     * @return InputStream of the file or null if it doesn't exist.
     */
    @Deprecated
    public @Nullable static InputStream getPreciseResourcesStream(String name) {
        InputStream stream = null;

        try {
            stream = new FileInputStream(Objects.requireNonNull(getResourcesFile(name)));
        } catch (FileNotFoundException | NullPointerException e) {
            Main.log(Level.SEVERE, "Trying to load a resources file that doesn't exist: " + name);
        }

        return stream;
    }

    public @Nullable static InputStream getResourcesStream(String name) {
        try {
            if(Objects.requireNonNull(IdeMain.class.getClassLoader().getResource(name.split("/")[0])).toURI().getScheme().equals("jar")) {
                return getResourcesStreamJar(name);
            } else {
                return getResourcesStreamIde(name);
            }
        } catch (URISyntaxException | NullPointerException e) {
            Main.log(Level.SEVERE, "Error parsing the possibility of a Jar file.");
            return null;
        }
    }

    private @Nullable static InputStream getResourcesStreamJar(String name) {
        try {
            JarFile jar = new JarFile(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            return jar.getInputStream(jar.getJarEntry(name));
        } catch (IOException e) {
            Main.log(Level.SEVERE, "Failed to load Jar file.");
            return null;
        }
    }

    private @Nullable static InputStream getResourcesStreamIde(String name) {
        URI uri = null;

        try {
            uri = Objects.requireNonNull(IdeMain.class.getClassLoader().getResource(name.split("/")[0])).toURI();
            new FileInputStream(uri.getPath());
        } catch (URISyntaxException | NullPointerException e) {
            Main.log(Level.SEVERE, "Trying to load a resources file with an invalid URI: " + name);
            return null;
        } catch (FileNotFoundException e) { // file wasn't found, it is a directory
            assert uri != null; // can make this assertion because if URI is null, a NullPointerException will be thrown and caught, ending the method earlier.

            try {
                Main.log(Level.INFO, "Loading resources file %s.", name);
                return new FileInputStream(new URI(uri.getPath() + "/" + name.split("/")[1]).getPath());
            } catch (URISyntaxException ex) {
                Main.log(Level.SEVERE, "Trying to load a resources file with an invalid URI: " + name);
                return null;
            } catch (FileNotFoundException fileNotFoundException) {
                Main.log(Level.SEVERE, "Trying to load a resources file that doesn't exist.");
                return null;
            }
        }

        return null;
    }

    /**
     * Get a file from the maven "resources folder" includes one level deep subfolders.
     * @param name File name to search for.
     * @return File or null if file doesn't exist.
     * @deprecated Method does not work in a Jar file.
     * @see #getResourcesStream(String)
     */
    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public @Nullable static File getResourcesFile(String name) {
        File file;

        try {
            URI uri = Objects.requireNonNull(IdeMain.class.getClassLoader().getResource(name.split("/")[0])).toURI();

            file = new File(uri);
        } catch (URISyntaxException | NullPointerException e) {
            Main.log(Level.SEVERE, "Trying to load a resources file that doesn't exist: " + name);
            return null;
        }

        Main.log(Level.INFO, "Loading resources file %s.", name);

        if(file.isFile())
            return file;
        else // is a directory
            return new File(file.getPath() + "/" + name.split("/")[1]);
    }

    /**
     * Get a file from the maven "resources folder"
     * @param name File name to search for.
     * @return File or null if file doesn't exist.
     * @deprecated Only gets resource files one level deep. No subfolders.
     * @see #getResourcesStream(String)
     */
    @Deprecated
    public @Nullable static File getPreciseResourceFile(String name) {
        URL url = Main.class.getClassLoader().getResource(name);

        if(url == null)
            Main.log(Level.SEVERE, "Trying to load a resources file that doesn't exist: " + name);

        Main.log(Level.INFO, "Loading resources file %s.", name);

        try {
            assert url != null;
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            Main.log(Level.SEVERE, "Trying to load a resources file that doesn't exist: " + name);
        }

        // if a file doesn't exist, then return null as a default
        return null;
    }

    /**
     * Return the folder path of the proper cache location for Paint.
     * Works on Windows, macOS, and Linux.
     * Follows XDG standards where applicable.
     * @return String path of the folder e.g.Always has a trailing slash. Returns null when OS is not supported.
     */
    public static @Nullable String getCacheFolder() {
        String os = System.getProperty("os.name");
        String home = System.getProperty("user.home") + "/";

        // adapted from: https://stackoverflow.com/a/31547504
        if(os.contains("win")) {
            return home + "AppData\\Local\\Paint\\";
        } else if(os.contains("nix") || os.contains("nux") || os.contains("aix")) {
            return home + ".cache/paint/";
        } else if(os.contains("mac")) {
            return home + "Library/Caches/net.nathanharmon.paint/";
        } else {
            Main.log(Level.WARNING, "Running on an unsupported operating system. Some file functionality will not work.");
            return null;
        }
    }

    /**
     * TODO:
     * Use a Python script to convert image formats not supported by the Image class.
     * Supported Formats: HEIC, WebP, SVG, FF, GIF, BMP, JPG, PNG
     * @param file The image file to be converted.
     * @return An image object from the file. Null when format is unsupported.
     */
    public @Nullable Image getImageFromFormats(@NotNull File file) {
        return switch (FilenameUtils.getExtension(file.toString())) {
            case "png", "jpg", "jpeg", "bmp", "gif" -> new Image(file.toURI().toString());
//            case "webp":
//                return new Image():
//            case "ff":
//                return new Image();
//            case "heic":
//                return new Image();
//            case "svg":
//                return new Image();
            default -> null;
        };
    }

    /**
     * Set the icon for the give stage.
     * @param stage The stage to modify.
     */
    public static void setIcon(Stage stage) {
        InputStream stream = getResourcesStream("images/brush.png");
        if(stream != null)
            stage.getIcons().add(new Image(stream));
    }
}
